import React, {useState} from "react";
import Button from "../../user/Components/Button";
import {NavLink} from "react-router-dom";


const Project = ({projectID,name,lastUpdate}) => {

    const changeProjectKey = () => {
        localStorage.setItem('key',projectID);
    };

    const [copied, setCopied] = useState(false);

    const deleteProject = async () => {
        const response = await fetch(`https://finaldan.finaldan.monster/projectData/${projectID}`,{
            method: 'DELETE',
            headers:{
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
        });
        const result = await response.json();
        const responseDataPr = await fetch(`https://finaldan.finaldan.monster/deletePr/${localStorage.getItem('userId')}`,{
            method: 'DELETE',
            headers:{
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'x-access-token': localStorage.getItem('accessToken'),
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({projectID}),
        });
        const resDataPr = await responseDataPr.json();
        window.location.reload();
    };

    const copyLink = () => {
        setCopied(true);
        navigator.clipboard.writeText(`https://finaldan.finaldan.monster/user/${projectID}/home`);
    }

    return(
        <div className='project'>
            <i className="far fa-copy copy" onMouseOut={() => setCopied(false)} onClick={copyLink}></i>
            <p className={`placeholder ${copied && 'copied'}`}>{(copied) ? 'Copied!' : 'Copy user link'}</p>
            <h3 className='project__h3'><i className="fas fa-angle-left"></i>{name} <p className='project__slash'>/</p><i className="fas fa-angle-right"></i></h3>
            <div className='project__date-box'>
                <p>Last update: {lastUpdate}</p>
            </div>
            <div className='project__button-box'>
                <NavLink className='project__edit' onClick={changeProjectKey} to={`/admin/${projectID}/home`} ><i className="fas fa-edit"></i></NavLink>
                <Button className='project__delete' onClick={deleteProject}><i className="fas fa-trash-alt"></i></Button>
            </div>
        </div>
    )
};


export default Project;